"""
fibonnaci.py
Using recursion, computes the terms and total of a
Fibonnaci sequence of N terms input by the user

Author: Richard Harrington
Date: 10/23/2013
"""
def fib(terms,term="",minusOne="",total=""):
    if minusOne is "":
        minusOne=1
    if term is "":
        term=0
    if total is "":
        total=0
    total+=term
    temp=term
    term+=minusOne
    minusOne=temp
    print(terms,term,total)
    if terms>0:
        return fib((terms-1),term,minusOne,total)

fib(int(input("enter a number:\n")))
